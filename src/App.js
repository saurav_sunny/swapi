import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from 'react-router-dom'
import axios from 'axios'
import Search from './Search'


const fakeAuth = {
  isAuthenticated: false,
  authenticate(cb) {
    this.isAuthenticated = true
    setTimeout(cb, 100)
  },
  signout(cb) {
    this.isAuthenticated = false
    setTimeout(cb, 100)
  }
}


class Login extends React.Component {

  state = {
    redirectToReferrer: false
  }
  constructor(props) {
    super(props);
    this.signIn = this.signIn.bind(this);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.state = {
      username: '',
      password: '',
      customObject: null
    };
  }

  componentDidMount() {
    var customObject = {};
    const getStarData = (apiURL = '') =>
    axios.get(apiURL)
        .then(fragment => {
          if (fragment.data.next) {
            return getStarData(fragment.data.next)
              .then(nextFragment => fragment.data.results.concat(nextFragment))
          } else {
            return fragment.data.results;
          }
        });
    let apiURL = 'https://cors-anywhere.herokuapp.com/https://swapi.co/api/people/';
    getStarData(apiURL)
      .then((customObject) => {
        console.log('yahan');
        this.setState({ customObject });
      });
  }

  signIn() {
    console.log('this.state', this.state.customObject);

    // console.log('this.state.username', this.state.username);

    var userNameData = this.state.username;
    var passwordData = this.state.password;
    var usrData = this.state.customObject;

    for (let i = 0; i < usrData.length; i++) {
      if (usrData[i].name === userNameData && usrData[i].birth_year === passwordData) {
        fakeAuth.authenticate(() => {
          this.setState(() => ({
            redirectToReferrer: true
          }))
        })
        this.props.history.push('/search')
      }
    }
  }

  handleUsernameChange(e) {
    this.setState({ username: e.target.value })
  }
  handlePasswordChange(e) {
    this.setState({ password: e.target.value })
  }

  render() {

    const { from } = this.props.location.state || { from: { pathname: '/' } }
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer === true) {
      <Redirect to={from} />
    }
    return (
      <div>
      {
        this.state.customObject !== null ? 
        <div>
        <form className="form-signin">
          <h2 className="form-signin-heading">Please sign in</h2>
          <label> Username </label>
          <input type="text" onChange={this.handleUsernameChange} placeholder="Username" />
          <div>
          <label> Password </label>
          <input type="password" onChange={this.handlePasswordChange} placeholder="Password" required />
          </div>
          <div>
          <button className="btn btn-primary" onClick={this.signIn} type="button">Sign in</button>
          </div>
        </form>

      </div> 
      : <div className="loader"></div>
      }
      </div>
      

    )
  }
}


const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    fakeAuth.isAuthenticated === true
      ? <Component {...props} />
      : <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }} />
  )} />
)

const AuthButton = withRouter(({ history }) => (
  fakeAuth.isAuthenticated ? (
    <p>
      Welcome! <button onClick={() => {
        fakeAuth.signout(() => history.push('/login'))
      }}>Sign out</button>
    </p>
  ) : (
      <p></p>
    )
))

export default function AuthExample() {
  return (
    <Router>
      <div>
        <AuthButton />
        <Route path="/login" component={Login} />
        <PrivateRoute path='/search' component={Search} />
      </div>
    </Router>
  )
}
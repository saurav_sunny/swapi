import React, { Component } from 'react';
import axios from 'axios';
import Planet from './Planet';


class Search extends Component {

    constructor() {
        super();
        this.filterList = this.filterList.bind(this);
        this.state = {
            searchText: '',
            searchResultsItems: null,
            items: []
        }
    }

    filterList(event) {
        console.log('this item' + JSON.stringify(this.state.searchResultsItems));
        var updatedList = this.state.searchResultsItems;
        updatedList = updatedList.filter(function (item) {
            console.log('this fffffff' + JSON.stringify(item));
            return item.name.toLowerCase().search(event.target.value.toLowerCase()) !== -1;
        });

        this.setState({ items: updatedList });
    }

    componentDidMount() {
        const getPlanetData = (apiURL = '') =>
            axios.get(apiURL)
                .then(fragment => {
                    if (fragment.data.next) {
                        return getPlanetData(fragment.data.next)
                            .then(nextFragment => fragment.data.results.concat(nextFragment))
                    } else {
                        return fragment.data.results;
                    }
                });
        let apiURL = 'https://cors-anywhere.herokuapp.com/https://swapi.co/api/planets/';
        getPlanetData(apiURL)
            .then((customObject) => {
                console.log('yahan search');
                this.setState({ searchResultsItems: customObject });
            });
    }

    render() {
        return (
            <div>
                {
                    this.state.searchResultsItems !== null ? 
                <div className="filter-list">
                    <form>
                        <fieldset className="form-group">
                            <input type="text" className="form-control form-control-lg" placeholder="Search planet name.." onChange={this.filterList} />
                        </fieldset>
                    </form>
                    <List items={this.state.items} />
                </div> : 
                <div className="loader"></div>
                }
            </div>

        );
    }
}

class List extends Component {

    constructor() {
        super();
        this.state = {
            planetItem: null
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(planetItem) {
        console.log('this itemmmmmmm' + JSON.stringify(planetItem));
        this.setState({
            planetItem
        });
    }

    render() {
        return (
            <div>
                {
                    this.state.planetItem !== null
                        ?
                        <div>
                            <Planet planetinfo={this.state.planetItem} />
                        </div>
                        : <div>
                            {
                                this.props.items.map((item, key) => {
                                    return <div key={key} className="floating-box">
                                        <a href="#" onClick={() => this.handleClick(item)}> {item.name}</a>
                                    </div>
                                })
                            }
                        </div>
                }

            </div>
        )
    }
};

export default Search;
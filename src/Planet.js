import React, { Component } from 'react';

class Planet extends Component {

    render() {

        let planetinfo;
        planetinfo = this.props.planetinfo !== null ? this.props.planetinfo : planetinfo;
        console.log('planet dataaaaaaaaaa' + JSON.stringify(planetinfo));
        return (
            <div className="planet">
                <div className="planet-info">
                    <div className="planet-name">{planetinfo.name} </div>
                    <div className="planet-followers">
                        {planetinfo.rotation_period} 
                        {planetinfo.orbital_period}
                        {planetinfo.diameter}
                        {planetinfo.climate}
                        <div>
                        {planetinfo.gravity}
                        {planetinfo.terrain}
                        </div>
                        <div>
                        {planetinfo.surface_water}
                        {planetinfo.population}
                        {planetinfo.residents}
                        </div>
                        <div>
                        {planetinfo.films}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Planet;